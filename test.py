import os
import boto3
from botocore import endpoint

def test_here_is_my_test():
   environment = os.getenv("LOCALSTACK_HOSTNAME", "localhost")
   endpoint = 'http://' + environment + ':4566'

   client = boto3.client('s3',aws_access_key_id='test',aws_secret_access_key='test', endpoint_url=endpoint )

   client.create_bucket(Bucket='test')